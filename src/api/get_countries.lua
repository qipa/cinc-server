local logger = CinCLogger
local pairs = pairs
local country = country
local coalition = coalition
local table = table

--- Get the list of countries currently defined in DCS
-- This command should only ever be called once by a client once per mission
-- as this information does not change once a mission has started
local command = function (_)
  logger.info("Command get_countries called")

  local response = {}
 
  for _, country in pairs(country['by_country']) do
  
    if country then 
    local response_country = {}
      response_country['world_id'] = country['WorldID']
      response_country['name'] = country['Name']
      response_country['short_name'] = country['ShortName']
      response_country['coalition'] = coalition.getCountryCoalition(country['WorldID'])
      table.insert(response, response_country)
    end
  end
  
  return response
end

CinCServer.add_command('get_countries', command)