local logger = CinCLogger
local world = world
local ipairs = ipairs

local command = function (_)
  logger.info("Command get_airbases called")
  local data = world.getAirbases()
  local result = {}
  for i, v in ipairs(data) do
    local airbase_properties = {}
    airbase_properties["id"] = v:getID()
    airbase_properties["name"] = v:getName()
    airbase_properties["category"] = v:getDesc()['category'] 
    airbase_properties["coalition"] = v:getCoalition()            
    result[i] = airbase_properties
  end
  return result  
end

CinCServer.add_command('get_airbases', command)