local logger = CinCLogger
local coalition = coalition
local coord = coord
local JSON = loadfile("Scripts\\JSON.lua")()
local ipairs = ipairs

local command = function (arguments)
  logger.info('Command add_group called')
  
  -- The Cinc Java FX map uses Google Maps to get Lat/Long. It looks like it doesn't quite match up to
  -- what we see in DCS. So add some offsets to reduce the delta. These were fiddled with manually by
  -- testing unit spawning accuracy @ Lat/Long 42.24062040154821, 42.03635413606048} (Senaki Airfield
  -- west end of runway on the edge where it meets dirt path going south in Satellite view
  local X_OFFSET = 185
  local Y_OFFSET = 208 
  
  for i, v in ipairs(arguments['group_data']['units']) do
    local unit_vec3 = coord.LLtoLO(v['x'], v['y']) 
    v['x'] = unit_vec3['x'] + X_OFFSET
    v['y'] = unit_vec3['z'] - Y_OFFSET
  end
   
  -- coaltion.addGroup is supposed to return the created group. However in testing it does not return properly when
  -- creating an air unit in my experience (unit has an ID of 0). I may be doing something wrong here. 
  -- local group = coalition.addGroup(arguments['country_id'], arguments['group_category'], arguments['group_data'] )
  -- However, creating the group THEN getting it by name appears to work...
    coalition.addGroup(arguments['country_id'], arguments['group_category'], arguments['group_data'])
  local group = Group.getByName(arguments['group_data']['name'])

  if group and group['id_'] > 0 then
    local response = {}
    response['group_name'] = group:getName()
    response['group_id'] = group:getID()
    response['group_category'] = group:getCategory()
    return response  
  else
    error('Group not created successfully')
  end  
end

CinCServer.add_command('add_group', command)